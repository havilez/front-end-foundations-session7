// jQUERY verson of toggle menu
// $('#pull').on('click', function() {
// 	$('nav ul').slideToggle();
// 	return false;
// });


const menuButton = document.querySelector('#pull')
const menu = document.querySelector('nav ul')
menuButton.addEventListener('click', toggleMenu)

function toggleMenu(){
	menu.classList.toggle('show');
	event.preventDefault();
}


// VIDEO SWITCHER
// Synopsis: within the content-video section of html, 
// find the anchor tag which has class=active and remove the class. 
// Assumes only on anchor tag can be active at any given time.  
// Set the anchor tag clicked on by user with class set to ‘active’. 
// Finally retrieve url from href within selected anchor tag and assign it to src attribute of iframe. 
// This will allow user to play selected video within iframe.

// jQUERY version
// $('.content-video a').on('click',function(){
// 	$('.content-video a').removeClass('active');
// 	$(this).addClass('active');
// 	var videoToPlay = $(this).attr('href');
// 	$('iframe').attr('src',videoToPlay);
// 	console.log(videoToPlay);
// 	return false;
//  });


const iFrame = document.querySelector('iframe');
// retrieve all anchor tags within .content-video section of html
const videoLinks = document.querySelectorAll('.content-video a');
 // convert node list of linked video anchor tags to javascript array
const videoLinksArray = [...videoLinks];
 // add  click event handler to each video anchor tag
videoLinksArray.forEach( videoLink => videoLink.addEventListener('click', selectVideo ))

function selectVideo(){
	// deselect button by removing highlight of previously selected button
	removeActiveClass();
	
	// set cursor to active button
	this.classList.toggle('active');
	const videoToPlay = this.getAttribute('href');
	iFrame.setAttribute('src', videoToPlay);
	console.log(this)
	event.preventDefault()
}

function removeActiveClass(){
	videoLinksArray.forEach( videoLink => videoLink.classList.remove('active'))
}





 